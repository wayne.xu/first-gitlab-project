from flask import Flask, jsonify, request

test_data = {
    'ID_01': {
        'account': 'test_account_01',
        'password': 'test_password_01'
    },
    'ID_02': {
        'account': 'test_account_02',
        'password': 'test_password_02',
    },
    'ID_03': {
        'account': 'test_account_03',
        'password': 'test_password_03',
    }
}

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello World'

@app.route('/api/test_data', methods=["GET"])
def get_test_data():
    print(request.args)
    # _id = request.args['account_id']
    _id = request.args.get('account_id')
    print(f"request_param:\n account_id: {_id}")
    return jsonify(test_data[_id])

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, threaded=True)